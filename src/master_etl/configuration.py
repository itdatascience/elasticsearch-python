import yaml


def get_config(config_file_path):
    with open(config_file_path) as config_file:
        config = yaml.load(config_file)
    return config
