import json
import os
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from src.master_etl import configuration as config
from src.utils.db_utils import get_data
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def get_colnames_to_transform(cfg):
    """Get the list of columns for each transformation."""
    transformation_rules = {'stopwords': cfg['transformations']['stopwords'],
                            'combinations': cfg['transformations']['combinations'],
                            'filters': cfg['transformations']['filters']}
    return transformation_rules


def remove_stopwords(phrase):
    """Tokenize phrase and return it as a list of tokens that are not on the stopwords list."""
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(phrase)
    filtered_phrase = [w for w in word_tokens if w not in stop_words]
    return filtered_phrase


def create_tokens(phrase):
    """Create a list of all sequential tokens combinations for a given phrase."""
    combined_tokens_list = []
    if type(phrase) is list:
        split_phrase = phrase
    else:
        split_phrase = phrase.split(' ')
    for i in range(len(split_phrase)):
        for j in range(len(split_phrase)-i):
            combined_tokens_list.append(' '.join(split_phrase[j:(j+i+1)]))
    return combined_tokens_list


def create_filters(filters_colnames, original_record):
    """Create a string that concatenates all fields that should be used as filters."""
    filter_list = [original_record[col_name] for col_name in filters_colnames]
    concat_colname = '_'.join(filters_colnames)
    original_record['%s_concat' % concat_colname] = '_'.join(filter_list)
    # return for unittest purposes
    return original_record['%s_concat' % concat_colname]


def add_tokens(combined_colname, stopwords_colnames, original_record):
    """Create tokens combinations needed by Elasticsearch. Remove stopwords if necessary."""
    phrase = original_record[combined_colname]
    if combined_colname in stopwords_colnames:
        phrase = remove_stopwords(phrase)

    combined_tokens = create_tokens(phrase)
    original_record['%s_tokens' % combined_colname] = combined_tokens
    # return for unittest purposes
    return combined_tokens


def transform_data(data, transformation_rules):
    """Transforming data into Elasticsearch compatible."""
    for record in data:
        for combined_colname in transformation_rules['combinations']:
            add_tokens(combined_colname, transformation_rules['stopwords'], record)
        create_filters(transformation_rules['filters'], record)
    return data


def save_to_json(data):
    """Save the final dataset to newline-delimited json file."""
    with open('dataset.json', 'w') as f:
        for item in data:
            f.write('{ "index": {}}\n')
            f.write(json.dumps(item) + '\n')
    return 'File successfully saved'


if __name__ == "__main__":
    environment = os.getenv('ENV', 'dev')
    cfg = config.get_config('config_' + environment + '.yml')
    data = get_data(cfg)
    transform_rules = get_colnames_to_transform(cfg)
    transformed_data = transform_data(data, transform_rules)

    es = Elasticsearch(cfg['es_address'])
    bulk(es, transformed_data, index=cfg['es_index'], doc_type='doc')
    save_to_json(transformed_data)
