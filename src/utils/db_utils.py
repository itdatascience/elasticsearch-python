import pymysql
import sys


def create_connection(cfg):
    connection = None
    try:
        connection = pymysql.connect(host=cfg['db_connection']['host'],
                                     user=cfg['db_connection']['user'],
                                     passwd=cfg['db_connection']['passwd'],
                                     db=cfg['db_connection']['db'])
    except pymysql.Error as e:
        print(e)
        sys.exit(1)
    return connection


def get_query(cfg):
    query = cfg['db_extract']['query']
    return query


def get_data(cfg):
    connection = create_connection(cfg)
    cursor = connection.cursor()
    query = get_query(cfg)
    data = None
    try:
        cursor.execute(query)
        columns = cursor.description
        data = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    except pymysql.Error as e:
        print(e)
        sys.exit(1)
    return data
