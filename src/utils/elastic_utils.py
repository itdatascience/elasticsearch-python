import json
import subprocess
import pandas as pd
import numpy as np
import os
import logging

logger = logging.getLogger(__name__)


def read_mapping(mapping_file):
    with open(mapping_file, 'r') as map_file:
        mapping = json.load(map_file)
    return mapping


def setup_index(es, index, mapping):
    if es.indices.exists(index):
        logger.info('Index exists - deleting')
        es.indices.delete(index)
    response = es.indices.create(index=index, body=json.dumps(mapping))
    return response['acknowledged']


def upload_data(upload_script, data_file, index, batch=None):
    if batch is None:
        cmd = subprocess.run([upload_script, data_file, index])
    else:
        cmd = batch_upload(upload_script, index, data_file, batch)
    return cmd


def batch_upload(upload_script, index, data_filepath, batch):
    part_filename = 'data_part.json'
    temp_df = pd.read_json(data_filepath)
    parts = np.array_split(temp_df, batch)
    prt_counter = 0
    logger.info('Processing {} parts...'.format(len(parts)))
    for part in parts:
        temp_df = pd.DataFrame(part)
        temp_df.to_json(part_filename, orient='records')
        cmd = subprocess.run([upload_script, part_filename, index])
        prt_counter += 1
        logger.info('Part {} uploaded'.format(prt_counter))
    os.remove(part_filename)
    return cmd


def reset_and_upload(es, index, upload_script, mapping_file, data_filepath, batch=None):
    mapping = read_mapping(mapping_file)
    response = setup_index(es, index, mapping)
    logger.info('Index setup completed: {}'.format(response))
    cmd = upload_data(upload_script, data_filepath, index, batch)
    logger.info('Data upload: {}'.format(cmd))
    return cmd
