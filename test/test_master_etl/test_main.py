import unittest
import numpy as np
from src.master_etl.main import *
from unittest.mock import patch


class TestMain(unittest.TestCase):

    def test_get_colnames_to_transform(self):
        cfg = {
            'transformations': {
                'stopwords': ['skill'],
                'combinations': ['jt', 'skill'],
                'filters': ['jt']
            }
        }

        rules = get_colnames_to_transform(cfg)

        self.assertTrue(np.array_equal(rules['stopwords'], ['skill']))
        self.assertTrue(np.array_equal(rules['combinations'], ['jt', 'skill']))
        self.assertTrue(np.array_equal(rules['filters'], ['jt']))

    @patch('src.master_etl.main.word_tokenize')
    @patch('src.master_etl.main.stopwords')
    def test_remove_stopwords(self, mock_sw, mock_wt):
        phrase = 'phrase1 and phrase2'

        mock_sw.words.return_value = ['and']
        mock_wt.return_value = phrase.split()

        filtered = remove_stopwords(phrase)

        self.assertTrue(np.array_equal(filtered, ['phrase1', 'phrase2']))
        mock_sw.words.assert_called_with('english')
        mock_wt.assert_called_with(phrase)

    def test_create_tokens(self):
        test_phrases = [
            ['phrase1', 'phrase2'],
            'phrase1 phrase2'
        ]

        valid_result = ['phrase1', 'phrase2', 'phrase1 phrase2']
        for test_phrase in test_phrases:
            with self.subTest(param=test_phrase):
                comb_tokens = create_tokens(test_phrase)
                self.assertTrue(np.array_equal(comb_tokens, valid_result))

    def test_create_filters(self):
        flt_colnames = ['skill', 'lang']
        orig_record = {
            'skill': 'Fluent',
            'lang': 'en_EN'
        }

        concat_colname = create_filters(flt_colnames, orig_record)
        self.assertTrue(concat_colname == 'Fluent_en_EN')

    @patch('src.master_etl.main.create_tokens')
    @patch('src.master_etl.main.remove_stopwords')
    def test_add_tokens(self, mock_rem_sw, mock_create_tok):
        combined_colname = 'skill'
        stopwords_colnames_cases = [
            ['jobtitle'],
            ['skill']
        ]
        orig_record = {'skill': 'phrase1 and phrase2'}

        mock_rem_sw.return_value = ['phrase1', 'phrase2']
        mock_create_tok.return_value = ['phrase1', 'phrase2', 'phrase1 phrase2']

        for sw_colname in stopwords_colnames_cases:
            with self.subTest(param=sw_colname):
                combined_tokens = add_tokens(combined_colname, sw_colname, orig_record)
                self.assertTrue(np.array_equal(
                    combined_tokens,
                    ['phrase1', 'phrase2', 'phrase1 phrase2']
                )
                )

    @patch('src.master_etl.main.add_tokens')
    @patch('src.master_etl.main.create_filters')
    def test_transform_data(self, mock_create_flt, mock_add_tok):

        data = [
            {'jt': 'ph1 and ph2', 'skill': 'ph3 and ph4'},
            #{'jt': 'ph5 and ph6', 'skill': 'ph7 and ph8'}
        ]

        trans_rules = {
            'stopwords': ['skill'],
            'combinations': ['skill'],
            'filters': ['jt']
        }

        result = transform_data(data, trans_rules)

        mock_add_tok.assert_called_with('skill', trans_rules['stopwords'], data[0])
        mock_create_flt(trans_rules['filters'], data[0])
        self.assertTrue(result[0]['jt'] == 'ph1 and ph2')
