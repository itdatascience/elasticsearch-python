import unittest
from src.utils.db_utils import *
import numpy as np


class TestMain(unittest.TestCase):

    def test_create_connection(self):
        cfg = {
            'db_connection': {
                'host': '127.0.0.1',
                'user': 'root',
                'passwd': 'root',
                'db': 'crawler2'
            }
        }
        connection = create_connection(cfg)
        self.assertTrue(connection)

    def test_get_query(self):
        cfg = {
            'db_extract': {
                'query': 'example_query'
            }
        }

        query = get_query(cfg)
        valid_query = 'example_query'

        self.assertTrue(np.array_equal(query, valid_query))

    def test_get_data(self):
        cfg = {
            'db_connection': {
                'host': '127.0.0.1',
                'user': 'root',
                'passwd': 'root',
                'db': 'crawler2'
            },
            'db_extract': {
            'query': 'SELECT * FROM new_skills'
            }
        }

        data = get_data(cfg)

        self.assertTrue(np.array_equal(data[0], {'jobtitles': 'Academic Advisor', 'skill': 'Team leadership'}))