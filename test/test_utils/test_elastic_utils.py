import unittest
import src.utils.elastic_utils as eu
import logging
import pandas as pd
from unittest.mock import patch
from elasticsearch import Elasticsearch

logging.basicConfig(level=logging.INFO)


class TestElasticUtils(unittest.TestCase):
    """Requires working ES cluster on localhost"""

    @classmethod
    def setUpClass(cls):
        cls.es = Elasticsearch()
        cls.index_name = 'python-unittest-index'
        cls.es.index(cls.index_name, 'doc', {})

    def test_setup_index(self):
        res = eu.setup_index(self.es, self.index_name, {})
        self.assertTrue(res)

    @patch('src.utils.elastic_utils.subprocess')
    def test_upload_data(self, mock_subpr):
        eu.upload_data('', '', '')
        mock_subpr.run.assert_called_with(['', '', ''])

    @patch('src.utils.elastic_utils.batch_upload')
    def test_upload_data_batch(self, mock_batch_up):
        eu.upload_data('', '', '', 100)
        mock_batch_up.assert_called_with('', '', '', 100)

    @patch('src.utils.elastic_utils.os')
    @patch('src.utils.elastic_utils.subprocess')
    @patch('src.utils.elastic_utils.pd')
    def test_batch_upload(self, mock_pd, mock_subp, mock_os):
        test_df = pd.DataFrame({
            'a': [1, 2, 3, 4],
            'b': [5, 6, 7, 8]
        })
        mock_pd.read_json.return_value = test_df

        eu.batch_upload('', '', 'data_fp', 2)
        mock_pd.read_json.assert_called_with('data_fp')
        mock_subp.run.assert_called_with(['', 'data_part.json', ''])
        mock_os.remove.assert_called_with('data_part.json')

    @patch('src.utils.elastic_utils.read_mapping')
    @patch('src.utils.elastic_utils.setup_index')
    @patch('src.utils.elastic_utils.upload_data')
    def test_reset_and_upload(self, mock_updata, mock_setupind, mock_readmp):

        mock_readmp.return_value = 'mapping'
        mock_updata.return_value = 'command'

        result = eu.reset_and_upload(
            'cluster', 'index', 'sh_script', 'map_fp', 'data_fp', 3)

        mock_readmp.assert_called_with('map_fp')
        mock_setupind.assert_called_with('cluster', 'index', 'mapping')
        mock_updata.assert_called_with('sh_script', 'data_fp', 'index', 3)
        self.assertEqual(result, 'command')

    @classmethod
    def tearDownClass(cls):
        cls.es.indices.delete(cls.index_name)
